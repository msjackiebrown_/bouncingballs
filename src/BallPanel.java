/**
 * 
 */

/**
 * @author msjac_000
 *
 */

import java.awt.Color;
import java.awt.Graphics;

	import javax.swing.Timer;
import javax.swing.JPanel;

import com.msjackiebrown.bouncingballs.Ball;

import java.awt.event.*;
import java.util.ArrayList;

	public class BallPanel extends JPanel implements MouseListener {
		
		private static final long serialVersionUID = 2455884470607513568L;
		private static ArrayList<Ball> balls;  
		private int refreshrate = 10;
		private Timer timer = new Timer(refreshrate, new TimerListener());
		private ArrayList<Thread> threads;
		
		
		public static ArrayList<Ball> getBalls() {
			return balls;
		}

		public static void setBalls(ArrayList<Ball> balls) {
			BallPanel.balls = balls;
		}

		private static int numberBalls=0;

		
		public BallPanel()
		{
			System.out.print("Ball Panel initalization..");
			
			
			 balls = new ArrayList<Ball>();
			 threads= new ArrayList<Thread>();
		
			 this.setBackground(Color.BLACK);
			 this.addMouseListener(this);

			 addBall(); 
			 System.out.println("Successful.");
		}
		
		
		private class TimerListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				repaint();
				
			}	
			
		}
		
		public static int getNumberBalls()
		{
			return numberBalls;
		}
		
		protected void paintComponent(Graphics g)
		{
			
			super.paintComponent(g);
			
			checkCollision();
			for (int index=0; index<balls.size(); index++)
			{
				
				balls.get(index).draw(g, this.getWidth(), this.getHeight());
			}
			
		}

		public Ball getBall(int index) {
			// TODO Auto-generated method stub
			
			return balls.get(index);
			
		}

		
		
		public void start() {

			//Start balls
			
			for (int index=0; index<threads.size(); index++)
			{
				Thread t= threads.get(index);
				if (!t.isAlive())
				{ t.start();}
			}
		
			timer.start();
			
		}
		
		public void addBall() {
			
			Ball b= new Ball();
			Thread t= new Thread(b);
			
			balls.add(numberBalls, b);
			threads.add(t);
			
			repaint();
			
			if (timer.isRunning())
			{
				t.start();
			}
			
			numberBalls+=1;
			System.out.println("Ball added...");
		}
		
		public void removeBall() 
		{	
			
			
			if (numberBalls>=1)
			{
				numberBalls-=1;
				balls.remove(numberBalls);
				threads.remove(numberBalls);	
			
		
			repaint();
			}
			
		}


		public void stop() {
			
			// Stop balls
			for (int index=0; index<balls.size(); index++)
			{
				balls.get(index).stop();
			}
			
			timer.stop();
			
		}
		
		private void checkCollision()
		{
			
			 for (int fb=0; fb<balls.size(); fb++)
			{
				for (int sb=fb+1; sb<balls.size(); sb++)
				{
					
					if (balls.get(fb).intersect(balls.get(sb)))
					{
						
						balls.get(fb).collision(balls.get(sb));	
					}
				}		
				
				} 
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		@Override
		public void mouseClicked(MouseEvent e) {
			
			
			// Toggle animation on click of mouse 
			
			if (!timer.isRunning())
			{
				this.start();
			}
			else
			{
				this.stop();
			}
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		

	}
		
		
		
