import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.msjackiebrown.bouncingballs.Ball;

/** Program Name: Bouncing Balls
 * @author Jacqueline D. Brown
 *
 */
public class BouncingBalls extends JFrame {

	private static final long serialVersionUID = 3002605696688613028L;
	
	private InfoPanel infoPanel;
	private BallPanel ballPanel;
	
	//Start button
	private JButton start;
	private JButton stop;
	private JButton increase;
	private JButton decrease;
	
	
	int currentBall = 0;
	
	
	public static void main(String[]args)
	{
		BouncingBalls frame = new BouncingBalls();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1600,800);
		frame.setVisible(true);
		frame.setTitle("Bouncing Balls");
		frame.setLayout(new BorderLayout());
	}
	
	
	public BouncingBalls()
	{
		
		//Initialize view
	
		ballPanel= new BallPanel();
		
		
		start=new JButton("Start");
		stop = new JButton("Stop");
		decrease=new JButton("-");
		increase=new JButton("+");
		
		
		JPanel control= new JPanel();
		control.add(decrease);
		control.add(increase);
		
		infoPanel= new InfoPanel();

		infoPanel.updateIndexes();
		
		JPanel startStop = new JPanel();
		startStop.add(start);
		startStop.add(stop);
		
		
		JPanel buttons = new JPanel();
		buttons.add(startStop);
		buttons.add(control);
		
		add(buttons, BorderLayout.SOUTH);
		add(ballPanel, BorderLayout.CENTER);
		add(infoPanel, BorderLayout.WEST);
		
		addListeners();
		
	}
	
	private void addListeners()
	{
		
		start.addActionListener(new ActionListener()
		{	

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ballPanel.start();
			}
		});
		
		stop.addActionListener(new ActionListener()
		{	

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ballPanel.stop();
			}
		});
		
		
		
		increase.addActionListener(new ActionListener()
		{
                        @Override
			public void actionPerformed(ActionEvent e) {
				
				ballPanel.addBall();
				infoPanel.updateIndexes();
			}
		
		});
		
		
		
		decrease.addActionListener(new ActionListener()
		{
		
                        @Override
			public void actionPerformed(ActionEvent e) {
			
			ballPanel.removeBall();
			infoPanel.updateIndexes();
		}
		});
		
		
		infoPanel.getBallindex().addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				infoPanel.update();
			}	
			
		});
		
		
		infoPanel.getRadius().addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				
				
				int currentBall = infoPanel.getBallindex().getSelectedIndex();
				ballPanel.getBall(currentBall).setRadius(infoPanel.getRadius().getValue());
				infoPanel.getPreview().repaint();
				
			}
			
			
		});
		
		infoPanel.getSpeed().addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e) {
				int currentBall = infoPanel.getBallindex().getSelectedIndex();
				ballPanel.getBall(currentBall).setSpeed(infoPanel.getSpeed().getValue());
				infoPanel.getPreview().repaint();
			}
		
		});
		
		
		infoPanel.getJslRed().addChangeListener(new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
		int currentBall = infoPanel.getBallindex().getSelectedIndex();
		Color color = new Color(infoPanel.getJslRed().getValue(),infoPanel.getJslGreen().getValue(), infoPanel.getJslBlue().getValue());
		ballPanel.getBall(currentBall).setColor(color);
		infoPanel.getPreview().repaint();
		
	}
 });
	
	
		infoPanel.getJslGreen().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				
				int currentBall = infoPanel.getBallindex().getSelectedIndex();
				Color color = new Color(infoPanel.getJslRed().getValue(),infoPanel.getJslGreen().getValue(), infoPanel.getJslBlue().getValue());
			ballPanel.getBall(currentBall).setColor(color);
			infoPanel.getPreview().repaint();
		}
	 });
		
		
		infoPanel.getJslBlue().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
			
			int currentBall = infoPanel.getBallindex().getSelectedIndex();
			Color color = new Color(infoPanel.getJslRed().getValue(),infoPanel.getJslGreen().getValue(), infoPanel.getJslGreen().getValue());
			ballPanel.getBall(currentBall).setColor(color);
			infoPanel.getPreview().repaint();
		}
	 });
	
		
		
		infoPanel.getRandomize().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				Color color = Ball.getRandomColor();
				int speed = Ball.getRandomSpeed();
				int radius = Ball.getRandomRadius();
				
				infoPanel.getJslBlue().setValue(color.getBlue());
				infoPanel.getJslGreen().setValue(color.getGreen());;
				infoPanel.getJslRed().setValue(color.getRed());
				
				infoPanel.getSpeed().setValue(speed);
				infoPanel.getRadius().setValue(radius);
				
		
			
		}
			
		});
	
		
	
	
	}	
		
	
	
	
}
	
	
	
	



