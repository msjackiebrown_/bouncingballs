
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.msjackiebrown.bouncingballs.Ball;

public class InfoPanel extends JPanel {
	
	private static final long serialVersionUID = -8625260983391989349L;
		
		final private static JSlider jslRed = new JSlider(0, 128);
		final private static JSlider jslGreen = new JSlider(0, 128);
		final private static JSlider jslBlue = new JSlider(0, 128);
		final private static JButton randomize = new JButton("Randomize");
		final private JComboBox<String> ballindex = new JComboBox<String>();
		
		
		// Create a panel to display the selected ball color
		final private JPanel preview = new GraphicPanel();
		final private JSlider radius= new JSlider(JSlider.HORIZONTAL,10,100,10);
		final private JSlider speed = new JSlider(JSlider.HORIZONTAL,1,20,10);
		
		public JComboBox<String> getBallindex() {
			return ballindex;
		}
		
		
		
		public JButton getRandomize()
		{
			return randomize;
		}
		
		
		private class GraphicPanel extends JPanel
		{	
			/**
			 * 
			 */
			private static final long serialVersionUID = -2385809474646878977L;

			protected void paintComponent(Graphics g)
			{

				super.paintComponent(g);
				
				this.setSize(300,300);
				int index = ballindex.getSelectedIndex();
				
				Ball  currentball = BallPanel.getBalls().get(index);
				
				int radius = currentball.getRadius();
				
				//Draw rectangle (yellow)
				g.setColor(Color.BLACK);
			
				int  x = this.getWidth()/2;
				int  y = this.getHeight()/2;
				

				
				 g.setColor(currentball.getColor());
				 g.fillOval(x-radius,y-radius,radius*2,radius*2);
				
				
			}	
		}
		
		
		
	
	public InfoPanel()
	{
		
		radius.setMajorTickSpacing(10);
		radius.setMinorTickSpacing(5);
		radius.setPaintTicks(true);
		radius.setPaintLabels(true);
	

		//Create the label table
		Hashtable<Integer, JLabel> radiusTable = new Hashtable<Integer, JLabel>();
		radiusTable.put( new Integer(10), new JLabel("Small") );
		radiusTable.put(new Integer(50), new JLabel("Medium"));
		radiusTable.put( new Integer(100), new JLabel("Large") );
		radius.setLabelTable( radiusTable );
		
		
			
		speed.setMajorTickSpacing(5);
		speed.setMinorTickSpacing(2);
		speed.setPaintTicks(true);
		speed.setPaintLabels(true);
		
		//Create the label table
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		labelTable.put( new Integer(1), new JLabel("Fast") );
		labelTable.put( new Integer(20), new JLabel("Slow") );
		speed.setLabelTable( labelTable );

		
		// Radius Panel 
		JPanel radiusPanel=new JPanel();
		radiusPanel.setLayout(new BoxLayout(radiusPanel, BoxLayout.X_AXIS));
		radiusPanel.add(new JLabel("Ball Size"));
		radiusPanel.add(radius);

		//Speed Panel 
		JPanel speedPanel=new JPanel();
		speedPanel.setLayout(new BoxLayout(speedPanel, BoxLayout.X_AXIS));
		speedPanel.add(new JLabel("Ball Speed"));
		speedPanel.add(speed);
		
		/* Color Panel */
		
		// Group sliders for selecting red, green, and blue colors
		JPanel jpSliders = new JPanel();
		jpSliders.setLayout(new BoxLayout(jpSliders, BoxLayout.Y_AXIS));
		jpSliders.add(jslRed);
		jpSliders.add(jslGreen);
		jpSliders.add(jslBlue);
		
		JPanel jpLabels = new JPanel();
		jpLabels.setLayout(new BoxLayout(jpLabels,BoxLayout.Y_AXIS));
		jpLabels.add(new JLabel("Red"));
		jpLabels.add(new JLabel("Green"));
		jpLabels.add(new JLabel("Blue"));	
		
		
		// Group jpLabels and jpSliders
		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBorder(BorderFactory.createTitledBorder("Select Color"));
		colorPanel.add(jpLabels, BorderLayout.WEST);
		colorPanel.add(jpSliders, BorderLayout.CENTER);
		
	
		JPanel property = new JPanel();
		property.setBorder(BorderFactory.createTitledBorder("Ball Properties"));
		property.setLayout(new BoxLayout(property, BoxLayout.Y_AXIS));
		property.add(ballindex);
		property.add(radiusPanel);
		property.add(speedPanel);
		property.add(colorPanel);
		
		//Add randomize button
		randomize.setPreferredSize(new Dimension(25,25));
		property.add(randomize);
		
			
		setLayout(new GridLayout(2,1));
	
		add(preview);
		add(property);
		
	}
	


	public JSlider getRadius() {
		return radius;
	}


	public JSlider getSpeed() {
		return speed;
	}



	public JSlider getJslRed() {
		return jslRed;
	}



	public JSlider getJslGreen() {
		return jslGreen;
	}



	public JSlider getJslBlue() {
		return jslBlue;
	}



	public JPanel getPreview() {
		return preview;
	}




	public void updateIndexes() {
		
		ballindex.removeAllItems();
		
		if (BallPanel.getNumberBalls()>0)
		
		{
			int currentIndex = ballindex.getSelectedIndex();
			for (int i = 0; i<BallPanel.getNumberBalls(); i++)
			{
			System.out.println("Updating indexes...." + i );
			String item = Integer.toString(i);
			ballindex.addItem(item);
			}
		
			ballindex.setEnabled(true);
			if (currentIndex!=-1)
			{
				ballindex.setSelectedIndex(currentIndex);
			}
			else
			{
			ballindex.setSelectedIndex(0);
			}	
		}
		else
		{
			ballindex.addItem("There are no balls available");
			ballindex.setEnabled(false);
		}
	}



	public void update() {
		
		
		// Update the info panel with current ball
		
		int index = ballindex.getSelectedIndex();
		
		if (index==-1) return;
		Ball  currentBall = BallPanel.getBalls().get(index);
		preview.repaint();
		radius.setValue(currentBall.getRadius());
		speed.setValue(currentBall.getSpeed());
		
		Color color = currentBall.getColor();
		
		jslRed.setValue(color.getRed());
		jslGreen.setValue(color.getGreen());
		jslBlue.setValue(color.getBlue());
		
		
		
		
		
	}

	
}

