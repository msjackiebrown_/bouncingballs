package com.msjackiebrown.bouncingballs;
/**
 * 
 */

/**
 * @author msjac_000
 *
 */


import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JComponent;

	public class Ball extends JComponent implements Runnable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1025461840951061037L;
		private final static int MAX_SIZE=50;
		private final static int MAX_SPEED = 5;
		
		private static int screenWidth;
		private static int screenHeight;
		
		private int radius;
		private int speed;
		private int xc;
		private int yc;
		private Color color;
		
		private int dx = 2;
		private int dy = 2;
		
		private AudioClip audioClip;
		

		public final static Color getRandomColor()
		{
			
			int redValue =	(int)(Math.random()*127) +1;
			int greenValue = (int)(Math.random()*127) +1;
			int blueValue = (int)(Math.random()*127)+1;
			
			return new Color(redValue,greenValue,blueValue);
			
		}
		public final static int getRandomRadius()
		{
			return (int) (Math.random() * MAX_SIZE)+ 10;
		}
		private final static int getRandomX()
		{
			return (int) ( Math.random() *screenWidth)+1;
			
		}
		private final static int getRandomY()
		{
			return (int) ( Math.random() *screenHeight)+1;
		}
		public final static int getRandomSpeed()
		{
			return  (int) (Math.random()* MAX_SPEED)+1;

		}
		
		 
		// Create  a random ball;
		public Ball()
		{
			this(getRandomRadius(), getRandomSpeed(), 
					getRandomX(), getRandomY(), getRandomColor());
			
		}
		
		// Create ball with specified parameters	
		public Ball(int radius, int speed, int x, int y, Color color)
		{
			this.radius=radius;
			this.speed=speed;
			this.xc=x;
			this.yc=y;
			this.color= color;
			intializeAudio();	
			
			

		}
		
		// Draw ball  of random speed and radius at a specified location
		public Ball(int xc, int yc) {
			
		this(getRandomRadius(), getRandomSpeed(), xc,yc, getRandomColor());
			
		}

		
		private void intializeAudio()
		{
			File bounceFile = new File("audio/bounceball.wav");
			
			URL urlForAudio = null;
			try {
				urlForAudio = bounceFile.toURI().toURL();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				
				System.out.println("Nope, still not working!");
				e.printStackTrace();
			}
			audioClip = Applet.newAudioClip(urlForAudio);

			


		}
		
		
		
		
		public void draw(Graphics g, int screenWidth, int screenHeight)
		{
			Ball.screenWidth= screenWidth;
			Ball.screenHeight= screenHeight;
			
			g.setColor(color);
			g.fillOval(xc-radius, yc-radius, radius*2, radius*2);
			
		}
		
		public int getRadius() {
			return radius;
		}
		public void setRadius(int radius) {
			this.radius = radius;
		}
		public int getSpeed() {
			return speed;
		}
		public void setSpeed(int speed) {
			this.speed = speed;
		}
		public int getXc() {
			return xc;
		}
		public void setXc(int x) {
			this.xc = x;
		}
		public int getYc() {
			return yc;
		}
		public void setYc(int y) {
			this.yc = y;
		}
		
		public Color getColor()
		{
			return color;
		}
	
		
		public void setColor(Color c)
		{
			this.color=c;
		}

		@Override
		/* Move ball */
		public void run() {
			
			;
			
			while (!Thread.interrupted())
			{
				
			if (xc-radius>screenWidth || xc-radius<0)
				{
					dx*=-1;
					audioClip.play();
					
					if (xc-radius>screenWidth) { xc=screenWidth; }
					if (xc-radius<0)	{ xc=radius; }
				
				
				}
			
				
			if (yc-radius>screenHeight|| yc-radius<0)
				
				{dy*= -1;
				audioClip.play();
				
				if (yc-radius>screenHeight) { yc=screenHeight; }
				if (yc-radius<0)	{ yc=radius; }	
				
				
				}
				
			//Adjust ball position
			
			xc+=dx;
			yc+=dy;
			

			
	
			try {
				
				Thread.sleep(speed);
				audioClip.stop();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
					
		}
		

		public void stop()
		{
			audioClip.stop();
			Thread.interrupted();
		}

		
		public boolean intersect(Ball b)
		{
		
			float xd = Math.abs(xc - b.getXc());
			float yd = Math.abs(yc - b.getYc());
			
			float sum = radius + b.getRadius();
			float sqrRadius = sum* sum;
			
			float distSqr = (xd*xd) +(yd*yd);
			
			if (distSqr<=sqrRadius)
			{
				
				
				return true;
			}
			
			
			return false;
		}

		public void collision(Ball b)
		{
		
			// Move balls
			dx*=-1;
			dy*=-1;
			int bdx = b.getDx();
			int bdy = b.getDy();
			b.setDx(bdx*=-1);
			b.setDy(bdy*=-1); 
			
		}

		

		
		
		public int getDx() {
			return dx;
		}

		public void setDx(int dx) {
			this.dx = dx;
		}

		public int getDy() {
			return dy;
		}

		public void setDy(int dy) {
			this.dy = dy;
		}

	
	

		}
		
	


